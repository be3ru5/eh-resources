# PATH

----

## STAGE 1 - _pre-security_

> approx 2 months

### Python

### Networking

### Linux

- environment
- command line
- bash scripting

### Windows

- environment
- cmd CL
- powershell CL
- powershell scripting

-----

## STAGE 2 - _intro to InfoSec_

> approx 1 month

### Theory from CEH

### Tools Introduction

### File Transferring

- linux to linux
- linux to windows
- windows to linux
- windows to windows

### Information Gathering

- Passive information gathering
- Active information gathering

### Port Redirection & tunneling

------

## STAGE 3 - _getting hands dirty_

> approx 4 months

### Vulnerability Scanning

### Exploitation

### Privilege Escalation

- linux privilege escalation
- windows privilege escalation

### OWASP top 10

> 2017

- injections
- broken authentication
- sensitive data exposure
- XML external entities (XXE)
- broken access control
- security misconfiguration
- cross site scripting
- insecure de-serialization
- using components with known vulnerabilities
- insufficient logging and monitoring

### Exploit Development

- stack based buffer-over flow

### TryHackMe

buy subscription and do following paths

- compTIA pentest+
- web fundamentals
- offensive pentesting
- _cyber defense (optional)_

-----

## STAGE 4 - _preparing for real world_

> approx 5  months

### Active Directory

### VulnHub _(linux and web apps)_

do as many boxes as you can for at least a month

### HackTheBox _(windows and AD)_

do as many boxes as you can for at least a month

### Firewall Bypass / Antivirus evasion

### Cloud Security

### API Basics
